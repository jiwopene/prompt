$(function () {
        $(".toolbox-blocks .block")
                .disableSelection()
                .draggable({
                        connectToSortable: $(".cmdline"),
                        helper: "clone",
                        revert: "invalid",
                        appendTo: "body",
                });
        var drop;
        $(".cmdline")
                .sortable()
                .droppable({
                        drop: drop = function (_, info) {
                                var $block = info.helper;
                                $block
                                        .dblclick(function () {
                                                $(".format-dialog")
                                                        .dialog("option", "x-block", $block)
                                                        .dialog("open");
                                        })
                                        .css({
                                                cursor: "pointer",
                                                width: "auto", // Unset width and height
                                                height: "auto",
                                        });
                                generateCode();
                        },
                });
        
        // "Drop" blocks that are devined in HTML source
        $(".cmdline .block").each(function () {
                drop(null, { helper: $(this) });
        });
                
        $(".format-dialog")
                .dialog({
                        title: "Format",
                        autoOpen: false,
                        modal: true,
                        buttons: [
                                { text: "Delete",
                                  click: function () {
                                        $(this)
                                                .dialog("close")
                                                .dialog("option", "x-block")
                                                        .remove();
                                                refreshCmdline($(".cmdline"));
                                                generateCode();
                                  }, },
                                { text: "Close",
                                  click: function () {
                                        $(this)
                                                .dialog("close");
                                  }, },
                        ],
                        open: function () {
                                $(this).find(".fg")
                                        .val($(this).dialog("option", "x-block").attr("data-fg"));
                                $(this).find(".bg")
                                        .val($(this).dialog("option", "x-block").attr("data-bg"));
                                $(this).find(".b")
                                        .prop("checked", $(this).dialog("option", "x-block").attr("data-style").indexOf("b") != -1);
                                $(this).find(".u")
                                        .prop("checked", $(this).dialog("option", "x-block").attr("data-style").indexOf("u") != -1);
                                $(this).find(".i")
                                        .prop("checked", $(this).dialog("option", "x-block").attr("data-style").indexOf("i") != -1);
                                $(this).find(".s")
                                        .prop("checked", $(this).dialog("option", "x-block").attr("data-style").indexOf("s") != -1);
                                $(this).find(".for-text, .for-date")
                                        .hide();
                                $(this).find(".for-" + $(this).dialog("option", "x-block").attr("data-type"))
                                        .show();
                                switch ($(this).dialog("option", "x-block").attr("data-type")) {
                                        case "text":
                                                $(this).find(".for-text-text")
                                                        .val($(this).dialog("option", "x-block").attr("data-content"));
                                                break;
                                        case "date":
                                                $(this).find(".for-date-format")
                                                        .val($(this).dialog("option", "x-block").attr("data-content"));
                                                break;
                                }
                        },
                        close: function () {
                                var $block = $($(this).dialog("option", "x-block"));
                                if ($block.parent().length) { // If the block is still in the DOM
                                        $block
                                                .attr("data-fg", $(this).find(".fg").val())
                                                .attr("data-bg", $(this).find(".bg").val())
                                                .attr("data-style",
                                                        ($(this).find(".b").prop("checked") ? "b" : "")
                                                        +
                                                        ($(this).find(".u").prop("checked") ? "u" : "")
                                                        +
                                                        ($(this).find(".i").prop("checked") ? "i" : "")
                                                        +
                                                        ($(this).find(".s").prop("checked") ? "s" : "")
                                                        );
                                        switch ($block.attr("data-type")) {
                                                case "text":
                                                        $block
                                                                .attr("data-content", $(this).find(".for-text-text").val());
                                                        break;
                                                case "date":
                                                        $block
                                                                .attr("data-content", $(this).find(".for-date-format").val());
                                                        break;
                                        }
                                }
                                refreshCmdline($(".cmdline"));
                                generateCode();
                        },
                });
                
        setInterval(function () {
                refreshCmdline($(".cmdline"));
        }, 1000);
        
        generateCode();
})

function refreshCmdline($cmdline) {
        $cmdline.find(".block").each(function () {
                var $block = $(this);
                
                switch ($block.attr("data-type")) {
                        case "text":
                                $block
                                        .text($block.attr("data-content"));
                                break;
                        case "date":
                                $block
                                        .text(new Date().strftime($block.attr("data-content")));
                                break;
                }
        });
}

function generateCode() {
        $(".codeout")
                .text(
                        "export PS1='" + generateCodeForCmdline($(".cmdline-ps1")) + "' " +
                        "PS2='" + generateCodeForCmdline($(".cmdline-ps2")) + "' " +
                        "PS4='" + generateCodeForCmdline($(".cmdline-ps4")) + "' " +
                        "PS4='" + generateCodeForCmdline($(".cmdline-ps0")) + "' " +
                        
                        "PS3=\"`echo -en '" +
                                generateCodeForCmdline($(".cmdline-ps3"))
                                        .replace(/\\\[/g, "")
                                        .replace(/\\\]/g, "")
                        + "'`\""
                );
}

function generateCodeForCmdline($cmdline) {
        var output = "";
        
        $cmdline.find(".block[data-style][data-fg][data-bg][data-type]").each(function () {
                var $block = $(this);
                
                output += "\\[\\e[";
                var flags = [0];
                if (-1 != +($block.attr("data-bg")))
                        flags.push(40+(+($block.attr("data-bg"))));
                if (-1 != +($block.attr("data-fg")))
                        flags.push(30+(+($block.attr("data-fg"))));
                if ($block.attr("data-style").indexOf("b") != -1)
                        flags.push(1);
                if ($block.attr("data-style").indexOf("u") != -1)
                        flags.push(3);
                if ($block.attr("data-style").indexOf("s") != -1)
                        flags.push(9);
                if ($block.attr("data-style").indexOf("i") != -1)
                        flags.push(3);
                output += flags.join(";");
                output += "m\\]";
                
                switch ($block.attr("data-type")) {
                        case "text":
                                output += $block.attr("data-content")
                                        .replace(/\\/g, "\\\\")
                                        .replace(/'/g, "\\'");
                                break;
                        case "hostname":
                                output += "\\h";
                                break;
                        case "fullname":
                                output += "\\H";
                                break;
                        case "date":
                                output += "\\D{" +
                                        $block.attr("data-content")
                                                .replace(/\\/g, "\\\\")
                                                .replace(/\}/g, "\\}")
                                        + "}";
                                break;
                        case "bell":
                                output += "\\a";
                                break;
                        case "user":
                                output += "\\u";
                                break;
                        case "workdir":
                                output += "\\w";
                                break;
                        case "fulldir":
                                output += "\\W";
                                break;
                        case "jobs":
                                output += "\\j";
                                break;
                        case "device":
                                output += "\\l";
                                break;
                        case "wrap":
                                output += "\\n\\r";
                                break;
                        case "shell":
                                output += "\\s";
                                break;
                        case "ver":
                                output += "\\v";
                                break;
                        case "verfull":
                                output += "\\V";
                                break;
                        case "nhist":
                                output += "\\!";
                                break;
                        case "ncmd":
                                output += "\\#";
                                break;
                        case "dollar":
                                output += "\\$";
                                break;
                }
        });
        
        return output;
}

Date.prototype.strftime = function (format) {
        return format
                // Composite
                .replace(/%[EO]?c/g, "%m/%d/%y %H:%M:%S")
                .replace(/%[EO]?D/g, "%m/%d/%y")
                .replace(/%[EO]?F/g, "%Y-%m-%d")
                .replace(/%[EO]?h/g, "%b")
                .replace(/%[EO]?r/g, "%I:%M:%S %p")
                .replace(/%[EO]?R/g, "%H:%M")
                .replace(/%[EO]?T/g, "%H:%M:%S")
                .replace(/%[EO]?x/g, "%m/%d/%y")
                .replace(/%[EO]?X/g, "%H:%M:%S")
                
                // Values
                .replace(/%[EO]?a/g, ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][this.getDay()])
                .replace(/%[EO]?a/g, ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][this.getDay()])
                .replace(/%[EO]?b/g, ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][this.getMonth()])
                .replace(/%[EO]?B/g, ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][this.getMonth()])
                .replace(/%[EO]?C/g, Math.floor((1900+this.getYear()) / 100.))
                .replace(/%[EO]?d/g, this.getDate().padRight(2, "0"))
                .replace(/%[EO]?e/g, this.getDate().padRight(2, " "))
                .replace(/%[EO]?G/g, 1900+this.getYear())
                .replace(/%[EO]?g/g, this.getYear() % 100)
                .replace(/%[EO]?H/g, this.getHours().padRight(2, "0"))
                .replace(/%[EO]?I/g, (((1+this.getHours()) % 12)-1).padRight(2, "0"))
                .replace(/%[EO]?j/g, (((+this) - +new Date(this.getFullYear(), 0, 2)) / 84600000).padRight(3, "0"))
                .replace(/%[EO]?k/g, this.getHours().padRight(2, " "))
                .replace(/%[EO]?l/g, (((1+this.getHours()) % 12)-1).padRight(2, " "))
                .replace(/%[EO]?m/g, (this.getMonth() + 1).padRight(2, "0"))
                .replace(/%[EO]?M/g, this.getMinutes().padRight(2, "0"))
                .replace(/%[EO]?p/g, (this.getHours() >= 12) ? "PM" : "AM")
                .replace(/%[EO]?p/g, (this.getHours() >= 12) ? "pm" : "am")
                .replace(/%[EO]?s/g, Math.floor((+this) / 1000))
                .replace(/%[EO]?S/g, this.getSeconds().padRight(2, "0"))
                .replace(/%[EO]?u/g, this.getDay() + 1)
                .replace(/%[EO]?U/g, this.getWeekNumber())
                .replace(/%[EO]?V/g, this.getWeekNumber())
                .replace(/%[EO]?w/g, this.getDay() + 1)
                .replace(/%[EO]?W/g, this.getWeekNumber())
                .replace(/%[EO]?y/g, this.getYear() % 100)
                .replace(/%[EO]?Y/g, this.getYear() + 1900)
                .replace(/%[EO]?z/g, Math.floor(this.getTimezoneOffset() / 100).padRight(2, "0") + (Math.abs(this.getTimezoneOffset()) % 100).padRight(2, "0"))
                .replace(/%[EO]?Z/g, "??T")
                .replace(/%%/g, "%");
};

// https://stackoverflow.com/a/6117889
Date.prototype.getWeekNumber = function(){
        var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
        var dayNum = d.getUTCDay() || 7;
        d.setUTCDate(d.getUTCDate() + 4 - dayNum);
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
        return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
};

Number.prototype.padRight = function (length, char) {
        if (char === undefined) char = "0";
        return ((""+char).repeat(length) + this).match(new RegExp(".".repeat(length) + "$"))[0];
};
