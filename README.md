![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

***This is a web application. Go to <https://jiwopene.gitlab.io/prompt/> if you want to use it.***

This is tool that generates prompt strings for `bash` (and other shells).

Prompt string is created using drag&drop interface. You can copy the command that configures the shell.